import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import ProductList from "./components/ProductList";
import ProductDetails from "./components/ProductDetails";
import "react-responsive-carousel/lib/styles/carousel.min.css";

function App() {
 return (
  <>
   <Router>
    <Routes>
     <Route path='/' element={<ProductList />} />
     <Route path='/products/:id' element={<ProductDetails />} />
    </Routes>
   </Router>
  </>
 );
}

export default App;
