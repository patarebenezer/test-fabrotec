import { useEffect, useState, useMemo } from "react";
import axios from "axios";
import Product from "./Product";
import FilterSort from "./FilterSort";

const API_URL = "https://dummyjson.com/products";
const CATEGORY_URL = "https://dummyjson.com/products/categories";

const ProductList = () => {
 const [products, setProducts] = useState([]);
 const [filterCategory, setFilterCategory] = useState("");
 const [sortBy, setSortBy] = useState("price-asc");
 const [categories, setCategories] = useState([]);

 useEffect(() => {
  axios
   .get(CATEGORY_URL)
   .then((response) => {
    setCategories(response.data);
   })
   .catch((error) => {
    console.error("Error fetching categories: ", error);
   });

  axios
   .get(API_URL)
   .then((response) => {
    setProducts(response.data.products);
   })
   .catch((error) => {
    console.error("Error fetching data: ", error);
   });
 }, []);

 // Use useMemo to memoize the filteredProducts array
 const filteredProducts = useMemo(() => {
  const filtered = filterCategory
   ? products.filter((product) => product.category === filterCategory)
   : products;

  const [sortField, sortOrder] = sortBy.split("-");
  filtered.sort((a, b) => {
   if (sortOrder === "asc") {
    return a[sortField] - b[sortField];
   } else {
    return b[sortField] - a[sortField];
   }
  });

  return filtered;
 }, [filterCategory, sortBy, products]);

 return (
  <div>
   <FilterSort
    filterCategory={filterCategory}
    setFilterCategory={setFilterCategory}
    sortBy={sortBy}
    setSortBy={setSortBy}
    categories={categories}
   />
   <div className='product-list'>
    {filteredProducts.length > 0 ? (
     filteredProducts.map((product) => (
      <Product key={product.id} product={product} />
     ))
    ) : (
     <p style={{ textAlign: "center", width: "100vw" }}>
      {filterCategory
       ? "No products match the selected category."
       : "Loading.."}
     </p>
    )}
   </div>
  </div>
 );
};

export default ProductList;
