const FilterSort = ({
 filterCategory,
 setFilterCategory,
 sortBy,
 setSortBy,
 categories,
}) => {
 return (
  <div className='filter-sort'>
   <div className='filter-select'>
    <label>Filter by Category:</label>
    <select
     value={filterCategory}
     onChange={(e) => setFilterCategory(e.target.value)}
    >
     <option value=''>All</option>
     {categories.map((category) => (
      <option key={category} value={category}>
       {category}
      </option>
     ))}
    </select>
   </div>
   <div className='sort-select'>
    <label>Sort by Price:</label>
    <select value={sortBy} onChange={(e) => setSortBy(e.target.value)}>
     <option value='price-asc'>Low to High</option>
     <option value='price-desc'>High to Low</option>
    </select>
   </div>
  </div>
 );
};

export default FilterSort;
