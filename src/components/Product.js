import { Link } from "react-router-dom";

const Product = ({ product }) => {
 return (
  <div className='product'>
   <img src={product?.thumbnail} alt={product.title} />
   <div className='product-details'>
    <h3>{product.title}</h3>
    <p>{product.description}</p>
    <p>Price: ${product.price}</p>
    <Link to={`/products/${product.id}`}>
     <button className='view-details-button'>View Details</button>
    </Link>
   </div>
  </div>
 );
};

export default Product;
