import { useEffect, useState, useMemo } from "react";
import { useParams, Link } from "react-router-dom";
import axios from "axios";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";

const API_URL = "https://dummyjson.com/products";

const ProductDetails = () => {
 const { id } = useParams();
 const [product, setProduct] = useState(null);

 // Fetch product data and memoize it
 const memoizedProduct = useMemo(() => {
  const cachedProduct = localStorage.getItem(`product_${id}`);

  if (cachedProduct) {
   return JSON.parse(cachedProduct);
  } else {
   axios
    .get(`${API_URL}/${id}`)
    .then((response) => {
     const fetchedProduct = response.data;
     localStorage.setItem(`product_${id}`, JSON.stringify(fetchedProduct));
     setProduct(fetchedProduct);
    })
    .catch((error) => {
     console.error("Error fetching product details: ", error);
    });
  }
 }, [id]);

 useEffect(() => {
  if (memoizedProduct) {
   setProduct(memoizedProduct);
  }
 }, [memoizedProduct]);

 if (!product) {
  return <div className='text-center'>Loading...</div>;
 }

 return (
  <div className='product-detail'>
   <Link to='/'>Back</Link>
   <h2>{product.title}</h2>
   <Carousel showThumbs={false} showArrows={true} autoPlay>
    {product.images.map((image, index) => (
     <div key={index}>
      <img className='img' src={image} alt={`Product ${index}`} />
     </div>
    ))}
   </Carousel>
   <p>{product.description}</p>
   <p>Price: ${product.price}</p>
   <p>
    Availability:{" "}
    <span className={product.availability ? "in-stock" : "out-of-stock"}>
     {product.availability ? "In Stock" : "Out of Stock"}
    </span>
   </p>
  </div>
 );
};

export default ProductDetails;
